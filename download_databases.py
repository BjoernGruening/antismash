#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

"""Script to download and install Pfam and ClusterBlast databases"""

import urllib2
import tarfile
import gzip
import os
import sys
import subprocess
import platform
import ctypes
from os import path

if sys.platform ==  ('win32') or sys.platform == ('darwin'):
    os.environ['EXEC'] = os.getcwd() + "\exec"
    os.environ['PATH'] = os.pathsep + os.environ['EXEC'] + os.pathsep + os.environ['PATH']

# pylint: disable=redefined-builtin
def execute(commands, input=None):
    "Execute commands in a system-independent manner"

    if input is not None:
        stdin_redir = subprocess.PIPE
    else:
        stdin_redir = None

    proc = subprocess.Popen(commands, stdin=stdin_redir,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    out, err = proc.communicate(input=input)
    retcode = proc.returncode
    return out, err, retcode
# pylint: enable=redefined-builtin

def get_remote_filesize(url):
    try:
        usock = urllib2.urlopen(url)
        dbfilesize = usock.info().get('Content-Length')
        if dbfilesize is None:
            dbfilesize = 0
    except urllib2.URLError:
        dbfilesize = 0
    dbfilesize = float(int(dbfilesize)) # db file size in bytes
    return dbfilesize

def get_free_space(folder):
    """ Return folder/drive free space (in bytes)"""
    if platform.system() == 'Windows':
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(folder), None, None, ctypes.pointer(free_bytes))
        return free_bytes.value
    else:
        return os.statvfs(folder).f_bfree * os.statvfs(folder).f_frsize

def check_diskspace(file_url):
    """ Check if sufficient disk space is available"""
    dbfilesize = int(get_remote_filesize(file_url))
    free_space = int(get_free_space("."))
    if free_space < dbfilesize:
        print 'ERROR: Insufficient disk space available (required: %d, free: %d).' % (dbfilesize, free_space)
        sys.exit()

def download_file(url, destination):
    """ Download a file"""
    filename = destination + os.sep + url.rpartition("/")[2]
    print "Downloading large file %s. Please be patient..." % (filename.rpartition(os.sep)[2])
    try:
        req = urllib2.urlopen(url)
    except urllib2.URLError:
        print 'ERROR: File not found on server.\nPlease check your internet connection.'
        sys.exit()
    CHUNK = 128 * 1024
    with open(filename, 'wb') as fp:
        while True:
            try:
                chunk = req.read(CHUNK)
                if not chunk:
                    break
                fp.write(chunk)
            except IOError:
                print 'ERROR: Download interrupted.'
                sys.exit()
    #Report download success
    print "Downloading %s finished successfully." % (filename.rpartition(os.sep)[2])
    return filename

def unzip_file(filename):
    """ Extract a TAR/GZ file"""
    newfilename = filename.rpartition(".gz")[0]
    try:
        gzfile = gzip.open(filename, 'rb')
        CHUNK = 128 * 1024
        with open(newfilename, 'wb') as fp:
            while True:
                try:
                    chunk = gzfile.read(CHUNK)
                    if not chunk:
                        break
                    fp.write(chunk)
                except IOError:
                    print 'ERROR: Unzipping interrupted.'
                    sys.exit()
    except gzip.zlib.error:
        print "ERROR: Error extracting %s. Please try to extract it manually." % (filename.rpartition(os.sep)[2])
        return
    print "Extraction of %s finished successfully." % (filename.rpartition(os.sep)[2])
    return newfilename

def untar_file(filename):
    """ Extract a TAR/GZ file"""
    try:
        tar = tarfile.open(filename)
        tar.extractall(path=filename.rpartition(os.sep)[0])
        tar.close()
    except tarfile.ReadError:
        print "ERROR: Error extracting %s. Please try to extract it manually." % (filename.rpartition(os.sep)[2])
        return
    print "Extraction of %s finished successfully." % (filename.rpartition(os.sep)[2])

def compile_pfam(filename):
    """ Compile a HMMer database with hmmpress"""
    command = ['hmmpress', '-f', filename]
    execute(command)

def delete_file(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

def main():
    #Download and compile PFAM database
    pfam_url = "ftp://ftp.sanger.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz"
    check_diskspace(pfam_url)
    pfam_destination = os.getcwd() + os.sep + os.sep.join(["antismash", "generic_modules", "fullhmmer"])
    filename = download_file(pfam_url, pfam_destination)
    filename = unzip_file(filename)
    compile_pfam(filename)
    delete_file(filename + ".gz")

    #Download and compile ClusterBlast database
    clusterblast_url = "https://bitbucket.org/antismash/antismash2/downloads/clusterblast.tar.gz"
    check_diskspace(clusterblast_url)
    clusterblast_destination = os.getcwd() + os.sep + os.sep.join(["antismash", "generic_modules", "clusterblast"])
    filename = download_file(clusterblast_url, clusterblast_destination)
    filename = unzip_file(filename)
    untar_file(filename)
    delete_file(filename)
    delete_file(filename + ".gz")

    # hmmpress the NRPS/PKS specific databases
    compile_pfam(path.join("antismash", "specific_modules", "nrpspks", "abmotifs.hmm"))
    compile_pfam(path.join("antismash", "specific_modules", "nrpspks", "dockingdomains.hmm"))
    compile_pfam(path.join("antismash", "specific_modules", "nrpspks", "ksdomains.hmm"))
    compile_pfam(path.join("antismash", "specific_modules", "nrpspks", "nrpspksdomains.hmm"))

    # hmmpress the smcog specific database
    compile_pfam(path.join("antismash", "generic_modules", "smcogs", "smcogs.hmm"))

if __name__ == "__main__":
    main()

